require 'test_helper'

class TweetTest < ActiveSupport::TestCase
  
  def setup
  	@tweet = Tweet.new(_id: "117406718039830528",
  										 text: "RT @brownisthecolor: For those of you who are despairing over lack of #occupywallstreet U.S. media coverage - it's all over Arabic TV news!",
  										 profile_image_url: "http://a0.twimg.com/profile_images/1497677107/BodhiTreeSymbolism_normal.jpg",
  										 created_at: "2011-09-24 01:15:17",
  										 geo: "N;",
  										 username: "KosmicJelli",
  										 user_id: "387329542",
  										 language: "en",
  										 source: "&lt;a href=&quot;http://twitter.com/&quot;&gt;web&lt;/a&gt;")
  end	

  test "should update created_at on save" do
  	assert @tweet.created_at.is_a?(String)
  	@tweet.save
  	assert @tweet.created_at.is_a?(DateTime)
  end

  test "should update hashtags on save" do
	end


end
