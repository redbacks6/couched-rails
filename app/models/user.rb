class User < CouchRest::Model::Base
	include TwitterApiHelper

	before_save :parse_user

	use_database 'users'

	property :_id, 								String
	property :username,						String
	property :tweet_count,				Integer
	property :created_at,					String
	property :description,				String
	property :lang,								String
	property :location,						String
	property :name,								String
	property :statuses_count, 		Integer
	property :time_zone,					String
	property :utc_offset,					String

	design do
		view :by_username
		view :by_created_at
	end

end
