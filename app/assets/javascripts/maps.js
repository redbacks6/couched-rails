function maps() {

  var div_country = '#country_sentiment_map';
  var div_state = '#us_sentiment_map';

  if($(div_country).length > 0){

    // Generate country map
    $.ajax({
       type: "GET",
       contentType: "application/json; charset=utf-8",
       url: db.concat('/_design/analytics/_view/country_sentiment?reduce=true&group=true&stale=update_after'),
       dataType: 'json',
       success: function (data) {
         map_data_country = manipulate_country_data(data);
         $.each(map_data_country, function () {
             this.flag = this.code.replace('UK', 'GB').toLowerCase();
         });
         country_sentiment_map(map_data_country, div_country);
       },
       error: function (result) {
           error();
       }
    });

    // Generate state map
    $.ajax({
       type: "GET",
       contentType: "application/json; charset=utf-8",
       url: db.concat('/_design/analytics/_view/state_sentiment?reduce=true&group=true&stale=update_after'),
       dataType: 'json',
       success: function (data) {
         map_data_state = manipulate_state_data(data);
         sentiment = arraytojson(data);
         state_sentiment_map(map_data_state, div_state);
       },
       error: function (result) {
           error();
       }
    });
  }
}

function manipulate_state_data(data) {
  // Helper function to manipulate data into map format
  var data_array = [];
  for (var i in data.rows) {
    ref = data.rows[i]
    if(isInArray(ref.key, states())) {
      average = ref.value.sum/ref.value.count;
      data_array.push({'code':ref.key,
                       'value':average.toFixed(2),
                       'sum':ref.value.sum.toFixed(2),
                       'count':ref.value.count});
    };
  };
  return data_array;
}

function manipulate_country_data(data) {
  // Helper function to manipulate data into map format
  var data_array = [];
  for (var i in data.rows) {
    ref = data.rows[i]
    average = ref.value.sum/ref.value.count;
    data_array.push({'code':ref.key,
                     'value':average.toFixed(2),
                     'sum':ref.value.sum.toFixed(2),
                     'count':ref.value.count});
  };
  return data_array;
}

function isInArray(value, array) {
  return array.indexOf(value) > -1;
}

function states() {
  return [
  'AK',
  'AL',
  'AR',
  'AZ',
  'CA',
  'CO',
  'CT',
  'DC',
  'DE',
  'FL',
  'GA',
  'HI',
  'IA',
  'ID',
  'IL',
  'IN',
  'KS',
  'KY',
  'LA',
  'MA',
  'MD',
  'ME',
  'MI',
  'MN',
  'MO',
  'MS',
  'MT',
  'NC',
  'ND',
  'NE',
  'NH',
  'NJ',
  'NM',
  'NV',
  'NY',
  'OH',
  'OK',
  'OR',
  'PA',
  'RI',
  'SC',
  'SD',
  'TN',
  'TX',
  'UT',
  'VA',
  'VT',
  'WA',
  'WI',
  'WV',
  'WY'
  ];
}
