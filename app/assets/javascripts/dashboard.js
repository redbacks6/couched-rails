function dashboard(){

  var div_words = 'word_cloud'
  var div_hashtags = 'hashtags_bar_chart'
  var div_mentions = 'mentions_bar_chart'
  var div_words_hash = '#word_cloud';
  var div_mentions_hash = '#mentions_bar_chart'
  var div_hashtags_hash = '#hashtags_bar_chart';

  if($(div_words_hash).length > 0 && data_recieved != true){

    data_recieved = true;
    //Populate the stopwords
    document.getElementById("stopwords").innerHTML = stopwords;
    // Generate mentions chart
    $.ajax({
       type: "GET",
       contentType: "application/json; charset=utf-8",
       url: db.concat('/_design/analytics/_list/sort_by_value/mentions_count?group=true&group_level=2&stale=update_after'),
       dataType: 'json',
       success: function (data) {
         mentions = data;
         var frequency_list = manipulate(mentions, 15, stopwords);
         mentions_chart(frequency_list, div_mentions_hash);
       },
       error: function (result) {
           error();
       }
    });

    // Generate hashtags chart
    $.ajax({
       type: "GET",
       contentType: "application/json; charset=utf-8",
       url: db.concat('/_design/analytics/_list/sort_by_value/hashtag_count?group=true&group_level=1&stale=update_after'),
       dataType: 'json',
       success: function (data) {
         hashtags = data;
         var frequency_list = manipulate(hashtags, 15, stopwords);
         hashtags_chart(frequency_list, div_hashtags_hash);
       },
       error: function (result) {
           error();
       }
    });

    // Generate word cloud
    $.ajax({
       type: "GET",
       contentType: "application/json; charset=utf-8",
       url: db.concat('/_design/analytics/_list/sort_by_value/word_count?group=true&stale=ok&stale=update_after'),
       dataType: 'json',
       success: function (data) {
         words = data;
         var frequency_list = frequencyList(words, stopwords);
         cloud(frequency_list, div_words, div_words_hash);

       },
       error: function (result) {
           error();
       }
    });
  } else if ( $(div_words_hash).length > 0 ) {

    //Populate the stopwords
    document.getElementById("stopwords").innerHTML = stopwords;

    clearDiv(div_words);
    clearDiv(div_hashtags);
    clearDiv(div_mentions);
    var hashtags_list = manipulate(hashtags, 15, stopwords);
    var mentions_list = manipulate(mentions, 15, stopwords);
    var frequency_list = frequencyList(words, stopwords);
    hashtags_chart(hashtags_list, div_hashtags_hash);
    mentions_chart(mentions_list, div_mentions_hash);
    cloud(frequency_list, div_words, div_words_hash);
  }

  function cloud(frequency_list, div_words, div_words_hash) {
    var width = document.getElementById(div_words).offsetWidth;
    var height = 400;
    var typeFace = 'Helvetica Neue';
    var minFontSize = 12;
    var maxFontSize = 100;
    var colors = d3.scale.category20b();

    var svg = d3.select(div_words_hash).append('svg')
       .attr('width', width)
       .attr('height', height)
       .append('g')
       .attr('transform', 'translate('+width/2+', '+height/2+')');

    var maxSize = max(frequency_list);
    var minSize = min(frequency_list);

    calculateCloud(frequency_list);

    function calculateCloud(wordCount) {
      d3.layout.cloud()
        .size([width, height])
        .words(wordCount)
        .rotate(function() { return ~~(Math.random()*2) * 90;}) // 0 or 90deg
        .font(typeFace)
        .fontSize(function(d) {
          var fontSize = minFontSize + (maxFontSize - minFontSize)*((d.size - minSize)/(maxSize - minSize));
          return fontSize
        })
        .on('end', drawCloud)
        .start();
    }

    function drawCloud(words) {
      var vis = svg.selectAll('text').data(words);
      vis.enter().append('text')
        .style('font-size', function(d) { return d.size + 'px'; })
        .style('font-family', function(d) { return d.font; })
        .style('fill', function(d, i) { return colors(i); })
        .attr('text-anchor', 'middle')
        .attr('transform', function(d) {
          return 'translate(' + [d.x, d.y] + ')rotate(' + d.rotate + ')';
        })
        .text(function(d) { return d.text; });
    }

  }
}
