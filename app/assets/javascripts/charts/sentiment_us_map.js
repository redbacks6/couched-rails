function state_sentiment_map(data, div_name) {

  // Instanciate the map
  $(div_name).highcharts('Map', {

    chart : {
        borderWidth : 0
    },

    title : {
        text : 'Average sentiment by State'
    },

    legend: {
        layout: 'horizontal',
        borderWidth: 0,
        backgroundColor: 'rgba(255,255,255,0.85)',
        floating: true,
        verticalAlign: 'top',
        y: 25
    },

    mapNavigation: {
        enabled: true
    },

    colorAxis: {
        min: -1,
        max: 1,
        type: 'linear',
        stops: [
      	  [0.3, '#0000ff'],
    	    [0.5, '#fffbbc'],
  	      [0.7, '#ff0000']
        ]
    },

    series : [{
        animation: {
            duration: 1000
        },
        data : data,
        mapData: Highcharts.maps['countries/us/us-all'],
        joinBy: ['postal-code', 'code'],
        dataLabels: {
            enabled: true,
            color: '#FFFFFF',
            format: '{point.code}'
        },
        name: 'Sentiment score',
        tooltip: {
            pointFormat: '<b>{point.name}</b><br>Average:{point.value} Count:{point.count}'
        }
    }]
  });
}
