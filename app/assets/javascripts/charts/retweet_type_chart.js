function retweet_type_chart(data, div_name) {

  $(div_name).highcharts({
      chart: {
          type: 'column'
      },
      title: {
          text: 'Content types of top retweets'
      },
      subtitle: {
          style: {
              fontSize: '13px',
              fontFamily: 'Verdana, sans-serif',
          }
      },
      xAxis: {
          type: 'category',
          labels: {
              style: {
                  fontSize: '13px',
                  fontFamily: 'Verdana, sans-serif',
              },
          }
      },
      yAxis: {
          min: 0,
          title: {
              text: '% top tweets containing contents type'
          },
          labels: {
            formatter: function () {
              var percentage = this.value.toPrecision(2)*100;
              return percentage + ' %';
            }
          }
      },
      legend: {
          enabled: false
      },
      tooltip: {
        pointFormat: 'Percentage of total tweets: <b>{point.y}</b>',
        formatter: function () {
          var percentage = this.y.toFixed(6)*100;
          console.log(this);
          return '<b>' + this.key + '</b><br>' + percentage.toFixed(1) + ' %';
        }
      },
      series: [{
          name: 'Tweet contents',
          data: data,
          dataLabels: {
              rotation: -90,
              color: '#FFFFFF',
              align: 'right',
              format: '{point.y}', // one decimal
              y: 10, // 10 pixels down from the top
              style: {
                  fontSize: '13px',
                  fontFamily: 'Verdana, sans-serif'
              }
          }
      }]
  });
}

function manipulate_retweet(data) {
  var data_obj = [];
  var photo = 0;
  var video = 0;
  var link = 0;
  var reply = 0;
  var quote = 0;
  var gif = 0;
  var total = 0;

  for (var i in data.rows) {
    ref = data.rows[i].value[2];
    total += 1;
    if (ref['photo']){
      photo += 1;
    };
    if (ref['video']){
      video += 1;
    };
    if (ref['link']){
      link += 1;
    };
    if (ref['reply']){
      reply += 1;
    };
    if (ref['quote']){
      quote += 1;
    };
    if (ref['gif']){
      gif += 1;
    };
  };
  data_obj = [['Photos',photo/total], ['Videos',video/total], ['Links',link/total], ['Replies',reply/total], ['Quotes',quote/total], ['Gifs',gif/total]];
  return data_obj;
}
