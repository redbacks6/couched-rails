function country_sentiment_map(data, div_name) {

  // Instanciate the map
  $(div_name).highcharts('Map', {

      title: {
          text: 'Average Sentiment by Country'
      },

      legend: {
          layout: 'horizontal',
          borderWidth: 0,
          backgroundColor: 'rgba(255,255,255,0.85)',
          floating: true,
          verticalAlign: 'top',
          y: 25
      },

      mapNavigation: {
          enabled: true,
          buttonOptions: {
              verticalAlign: 'bottom'
          }
      },

      tooltip: {
          backgroundColor: 'none',
          borderWidth: 0,
          shadow: false,
          useHTML: true,
          padding: 0,
          pointFormat: '<span class="f32"><span class="flag {point.flag}"></span></span>' +
              ' <b>{point.name}</b><br>Average:{point.value} Count:{point.count}',
          positioner: function () {
              return { x: 0, y: 250 };
          }
      },

      colorAxis: {
          min: -1,
          max: 1,
          type: 'linear',
          stops: [
            [0.3, '#0000ff'],
            [0.5, '#fffbbc'],
            [0.7, '#ff0000']
          ]
      },

      series : [{
          data : data,
          mapData: Highcharts.maps['custom/world'],
          joinBy: ['iso-a2', 'code'],
          name: 'Sentiment',
          states: {
              hover: {
                  color: '#BADA55'
              }
          }
      }]
  });
}
