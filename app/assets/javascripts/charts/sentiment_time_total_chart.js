function sentiment_time_total_chart(data, div_name) {

  $(div_name).highcharts({
      chart: {
          type: 'area'
      },
      colors: ['#64E572', '#FFF263', '#ff6666'],
      title: {
          text: 'Sentiment over time (total count)'
      },
      xAxis: {
          categories: data.categories,
          tickmarkPlacement: 'on',
          title: {
              enabled: false
          },
          labels: {
            formatter: function () {
              date = this.value;
              var output = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
              return output;
            },

          }
      },
      yAxis: {
          title: {
              text: 'Count'
          }
      },
      tooltip: {
          pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.percentage:.1f}%</b> ({point.y:,.0f})<br/>',
          shared: true
      },
      plotOptions: {
          area: {
              stacking: 'normal',
              lineColor: '#666666',
              lineWidth: 1,
              marker: {
                  lineWidth: 1,
                  lineColor: '#666666'
              }
          }
      },
      series: data.series
  });
}
