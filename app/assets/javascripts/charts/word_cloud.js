function frequencyList(data, stopwords) {
  var data_array = [];
  for (var i in data.rows) {
    var ref = data.rows[i];
    if(ref.key.length > 3 & stopwords.indexOf(ref.key) == -1) {
      data_array.push({"text":ref.key,"size":ref.value});
    };
  };
  return data_array;
}

function min(data) {
  var min = Infinity;
  for (var i in data) {
    if (data[i].size < min){
      var min = data[i].size;
    };
  };
  return min;
}

function max(data) {
  var max = 0;
  for (var i in data) {
    if (data[i].size > max){
      var max = data[i].size;
    };
  };
  return max;
}
