function mentions_chart(data, div_name) {

  $(div_name).highcharts({
      chart: {
          type: 'column'
      },
      title: {
          text: 'Most Mentions'
      },
      subtitle: {
          text: '',
          style: {
              fontSize: '13px',
              fontFamily: 'Verdana, sans-serif',
              textDecoration: 'underline',
              color: '#0000EE'
          }
      },
      xAxis: {
          type: 'category',
          labels: {
              rotation: -90,
              style: {
                  fontSize: '13px',
                  fontFamily: 'Verdana, sans-serif',
                  textDecoration: 'underline',
                  color: '#0000EE'

              },
              formatter: function () {
                  var url = window.location.origin;
                  //var id = point.id;
                  return '<a href="'+ url +'/users/' + this.value[1] + '">' +
                    this.value[0] + '</a>';
              }
          }
      },
      yAxis: {
          min: 0,
          title: {
              text: 'Count'
          }
      },
      legend: {
          enabled: false
      },
      tooltip: {
          pointFormat: 'Count of mentions: <b>{point.y}</b>'
      },
      series: [{
          name: 'Mentions',
          data: data,
          dataLabels: {
              enabled: true,
              rotation: -90,
              color: '#FFFFFF',
              align: 'right',
              format: '{point.y}', // one decimal
              y: 10, // 10 pixels down from the top
              style: {
                  fontSize: '13px',
                  fontFamily: 'Verdana, sans-serif'
              }
          }
      }]
  });
}
