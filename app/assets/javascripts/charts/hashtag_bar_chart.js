function hashtags_chart(data, div_name) {
    $(div_name).highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Top Hashtags'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -90,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif',
                    textDecoration: 'underline',
                    color: '#0000EE'

                },
                formatter: function () {
                  var url = window.location.origin;
                  return '<a href="'+ url +'/hashtags/' + this.value + '">' +
                      this.value + '</a>';
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Count'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Count of hashtags: <b>{point.y}</b>'
        },
        series: [{
            name: 'Hashtags',
            data: data,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
}
