function engagement(){

  var div_retweet_type = '#retweet_type_chart';

  if($(div_retweet_type).length > 0){

    // Generate retweet type chart
    $.ajax({
       type: "GET",
       contentType: "application/json; charset=utf-8",
       url: db.concat('/_design/analytics/_list/sort_array_by_value/retweets_advanced?group=true&stale=update_after'),
       dataType: 'json',
       success: function (data) {

         retweets = manipulate_retweet(data);
         retweet_type_chart(retweets, div_retweet_type);
       },
       error: function (result) {
           error();
       }
    });
  }
}
