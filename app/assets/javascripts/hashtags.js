function generateHashtagsChart() {

  var div_total = '#hashtag_sentiment_chart_total';
  var div_percentage = '#hashtag_sentiment_chart_percentage';
  var params = $(div_total).data('hashtag');

  if($(div_total).length > 0){
    var db_url = db.concat("/_design/analytics/_view/hashtag_count?group=true&startkey=%5B%22",params,"%22%5D&endkey=%5B%22",params,"%22,%7B%7D%5D&stale=update_after")

    $.ajax({
       type: "GET",
       contentType: "application/json; charset=utf-8",
       url: db_url,
       dataType: 'json',
       success: function (data) {
         var hashtag_sentiment = manipulate_sentiment(data);
         sentiment_time_total_chart(hashtag_sentiment, div_total);
         sentiment_time_percentage_chart(hashtag_sentiment, div_percentage);
       },
       error: function (result) {
         error();
       }
    });
  }
}
