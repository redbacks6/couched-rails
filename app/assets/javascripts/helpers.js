function manipulate(data, max_items, stopwords) {
  var data_array = [];
  var count = 0;
  for (var i in data.rows) {
    if (count > max_items) { break; }
    ref = data.rows[i];

    if (ref.key[1]) {
      if(stopwords.indexOf(ref.key[1]) == -1) {
        data_array.push({"name":[ref.key[1],ref.key[0]],"y":ref.value, "id":ref.key[0]})
        count += 1;
      };
    } else {
      if(stopwords.indexOf(ref.key[0]) == -1) {
        data_array.push({"name":ref.key[0],"y":ref.value})
        count += 1;
      };
    };
  };
  return data_array;
}

function arraytojson(array) {
  var json = {}
  for (var i in array.rows) {
    var ref = array.rows[i];
    var key = ref.key
    var value = ref.value
    json[key] = value;
  };
  return json;
}

function manipulate_sentiment(data) {
  // From this to match var sentiment below
  // {"key"=>["trump", "neg", 2016, 1, 1], "value"=>53}
  // {"key"=>["trump", "neutral", 2016, 1, 1], "value"=>964}
  // {"key"=>["trump", "pos", 2016, 1, 1], "value"=>273}

  //get categories
  var categories = [];
  for (var i in data.rows) {
    var ref = data.rows[i];
    var len = ref.key.length;
    var day = ref.key[len-1];
    var month = ref.key[len-2];
    var year = ref.key[len-3];
    var date = new Date(year,month,day);
    categories.push(date);
    };
  categories = uniq(categories);
  categories.sort(function(a, b){return a-b});

  //create object arrays for sentiment and initialise to zero
  
  var sentiment = {'series':[{'name': 'positive',
                              'data': new Array(categories.length).fill(0)},
                             {'name': 'neutral',
                              'data': new Array(categories.length).fill(0)},
                             {'name': 'negative',
                              'data': new Array(categories.length).fill(0)}
                            ],
                            'categories': categories};

  for (var i in data.rows) {
    var ref = data.rows[i];
    var len = ref.key.length;
    var day = ref.key[len-1];
    var month = ref.key[len-2];
    var year = ref.key[len-3];
    var date = new Date(year,month,day);
    var sentiment_val = ref.key[len-4];
    var value = ref.value;
    var index = categories.map(Number).indexOf(+date);

    // Add the value to the correct data array at the correct index
    switch (sentiment_val) {
    case 'pos':
        sentiment.series[0].data[index] = value;
        break;
    case 'neutral':
        sentiment.series[1].data[index] = value;
        break;
    case 'neg':
        sentiment.series[2].data[index] = value;
        break;
    };
  };
  return sentiment;
}

function manipulate_sentiment_tweets(data) {
  // Covert to only count unique keys
  // {"key"=>["trump", "neg", 2016, 1, 1, "tweet1"], "value"=>53}
  // {"key"=>["trump", "neutral", 2016, 1, 1, "tweet2"], "value"=>964}
  // {"key"=>["trump", "pos", 2016, 1, 1, "tweet3"], "value"=>273}

  var tweet_sentiment = new Object();
  var final = {rows:[]}
  for (var i in data.rows) {
    var ref = data.rows[i];
    var id = [ref.key[0],ref.key[1],ref.key[2],ref.key[3], ref.key[4]];
    if (tweet_sentiment[id] >= 1) {
      tweet_sentiment[id] +=1 ;
    } else {
      tweet_sentiment[id] = 1;
    }
  }
  for (var i in tweet_sentiment) {
    var keys=i.split(",")
    final.rows.push({key:[keys[0], keys[1], parseInt(keys[2]),parseInt(keys[3]),parseInt(keys[4])], value:tweet_sentiment[i]});
  }
  return manipulate_sentiment(final);
}

function uniq(a) {
    var seen = {};
    return a.filter(function(item) {
        return seen.hasOwnProperty(item) ? false : (seen[item] = true);
    });
}

function error() {
    console.log("error")
}

function clearDiv(divID) {
  document.getElementById(divID).innerHTML = "";
}
