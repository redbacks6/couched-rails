function users() {

  var div_mentions_percent = '#mentions_sentiment_time_percentage_chart';
  var div_mentions_total = '#mentions_sentiment_time_total_chart';
  var div_tweets_percentage = '#tweets_sentiment_time_percentage_chart';
  var div_tweets_total = '#tweets_sentiment_time_total_chart';
  var div_retweet_type = '#retweet_type_chart';
  var user = $('#data').data('user');

  if($(div_mentions_percent).length > 0){
    var mentions_url = db.concat("/_design/analytics/_view/mentions_count?group=true&startkey=%5B%22",user,"%22%5D&endkey=%5B%22",user,"%22,%7B%7D%5D&stale=update_after")
    var tweets_url = db.concat("/_design/analytics/_view/unique_tweets?group=true&startkey=%5B%22",user,"%22%5D&endkey=%5B%22",user,"%22,%7B%7D%5D&stale=update_after")

    $.ajax({
       type: "GET",
       contentType: "application/json; charset=utf-8",
       url: mentions_url,
       dataType: 'json',
       success: function (data) {
         mention_sentiment = manipulate_sentiment(data);
         sentiment_time_percentage_chart(mention_sentiment, div_mentions_percent);
         sentiment_time_total_chart(mention_sentiment, div_mentions_total);
       },
       error: function (result) {
         error();
       }
    });

    $.ajax({
       type: "GET",
       contentType: "application/json; charset=utf-8",
       url: tweets_url,
       dataType: 'json',
       success: function (data) {
         tweet_sentiment = manipulate_sentiment_tweets(data);
         sentiment_time_percentage_chart(tweet_sentiment, div_tweets_percentage);
         sentiment_time_total_chart(tweet_sentiment, div_tweets_total);
       },
       error: function (result) {
         error();
       }
    });

    // Generate retweet type chart
    $.ajax({
       type: "GET",
       contentType: "application/json; charset=utf-8",
       url: db.concat("/_design/analytics/_list/sort_array_by_value/retweets_advanced?group=true&startkey=%5B%22",user,"%22%5D&endkey=%5B%22",user,"%22,%7B%7D%5D&stale=update_after"),
       dataType: 'json',
       success: function (data) {
         retweets = manipulate_retweet(data);
         retweet_type_chart(retweets, div_retweet_type);
       },
       error: function (result) {
           error();
       }
    });
  }
}
