window.onload = function() {

  dashboard();
  generateHashtagsChart();
  maps();
  users();
  engagement();

}

function updateCharts() {
  stopwords = document.getElementById("stopwords").value.split(",");
  dashboard();
  generateHashtagsChart();
  maps();
  users();
  engagement();
}
