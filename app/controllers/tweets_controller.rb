class TweetsController < ApplicationController

  before_action :set_db, only: [:show]

  # GET /tweets
  # GET /tweets.json
  # def index
  #   @tweets = Tweet.all
  # end

  # GET /tweets/1
  # GET /tweets/1.json
  def show
    @doc = request_data("/_view/by_tweet_id?&include_docs=true&limit=1&key=%22#{params[:id]}%22&stale=update_after")['rows'][0]['doc']
    @tweet = params[:id] == @doc['id_str'] ? @doc : @doc['retweeted_status']
  end

end
