class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  require 'json'
  require 'uri'

	private
	  def set_db
	    couch_server = 'http://115.146.95.29:5984'
	    database = '/maydebout'
      # database = '/realdonaldtrump'
	    design_doc = '/_design/analytics'
	    @connection = couch_server+database+design_doc
	    @connection_raw = couch_server+database
	  end

    def get_tweet_html(tweet_data)
			tweets_html = []
			tweet_data.each do |tweet|
				begin
					tweet_obj = get_tweet(tweet['key'][1].to_str)
					tweets_html.push(tweet_obj)
				rescue
					print "Error on tweet id: " + tweet['key'][1] +"\n"
				end
			end
			return tweets_html
		end

    def request_data(url)
      set_db
      JSON.parse(RestClient.get(@connection+url), {accept: :json})
    end

end
