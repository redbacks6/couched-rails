class StaticPagesController < ApplicationController

	include TwitterApiHelper

  before_action :set_db, only: [:engagement, :raw_data]

	def dashboard
	end

	def us_map
	end

	def photos
		@photos = request_data("/_list/sort_by_value/photo_count?group=true&stale=update_after")['rows']
	end

	def engagement
		@retweets = request_data("/_list/sort_array_by_value/retweet_count?group=true&stale=update_after")['rows']
		@retweets_html = get_tweet_html(@retweets[0..14])
		@favourites = request_data("/_list/sort_array_by_value/favourite_count?group=true&stale=update_after")['rows']
		@favourites_html = get_tweet_html(@favourites[0..14])
	end

	def raw_data
		@db = @connection
		@design_document = request_data("")
	end

end
