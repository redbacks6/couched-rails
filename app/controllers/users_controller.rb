class UsersController < ApplicationController

  include TwitterApiHelper
  before_action :set_db

  # GET /users/1
  # GET /users/1.json
  def show
    @retweets = request_data("/_list/sort_array_by_value/retweet_count?group=true&startkey=[%22#{params[:id]}%22]&endkey=[%22#{params[:id]}%22,%7B%7D]&stale=update_after")['rows']
		@retweets_html = get_tweet_html(@retweets[0..9])
    @user = find_user(params[:id])
  end

  def index
    @users = request_data("/_list/sort_by_value/mentions_count?group=true&group_level=2&stale=update_after")['rows']
  end



end
