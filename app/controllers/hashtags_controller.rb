class HashtagsController < ApplicationController

  include TwitterApiHelper
  before_action :set_db, only: [:show]

  def show
    @hashtags = request_data("/_view/hashtag_count?group=true&startkey=[%22#{params[:id]}%22]&endkey=[%22#{params[:id]}%22,%7B%7D]&stale=update_after")['rows']
  end

  def index
    @hashtags = request_data("/_list/sort_by_value/hashtag_count?group=true&group_level=1&stale=ok")['rows']
  end



end
