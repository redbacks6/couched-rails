module TwitterApiHelper

	def home_timeline
		$twitter.home_timeline[0..4]
	end

	def find_user(user_id)
		$twitter.user(user_id.to_i)
	end

	def get_tweet(tweet_id)
		$twitter.oembed(tweet_id)
	end

end
