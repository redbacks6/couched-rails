Rails.application.routes.draw do
  resources :users
  resources :tweets
  resources :hashtags

  root 												 'static_pages#dashboard'
  get			'dashboard' 			=> 'static_pages#dashboard'
  get     'maps'            => 'static_pages#maps'
  get     'engagement'      => 'static_pages#engagement'
  get     'raw_data'        => 'static_pages#raw_data'
  get     'photos'          => 'static_pages#photos'

  get 'static_pages/hashtags', :defaults => { :format => 'json' }

end
