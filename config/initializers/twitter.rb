require 'twitter'

$twitter = Twitter::REST::Client.new do |config|
  config.consumer_key = ENV["TWITTER_CONSUMER_KEY1"]
  config.consumer_secret = ENV["TWITTER_CONSUMER_SECRET1"]
  config.access_token = ENV["TWITTER_ACCESS_TOKEN1"]
  config.access_token_secret = ENV["TWITTER_ACCESS_SECRET1"]
end
