# Twitter Analytics Project

## Overview
This Rails application has been designed to allow exploration of a twitter conversation stored on a couchdb database.

### Table of Contents
**[Installation Instructions](#installation-instructions)**  
**[Usage Instructions](#usage-instructions)**  
**[Troubleshooting](#troubleshooting)**  
**[Compatibility](#compatibility)**  

## Installation instructions
The application can be deployed anywhere that supports Rails application.  I chose to use [Dokku](http://dokku.viewdocs.io/dokku/) to allow deployment to a vanilla Linux Ubunutu machine on the Nectar Research Cloud.  [Heroku](https://www.heroku.com) may be another suitable deployment option.

### Configuration
While I aimed to keep configuration to a minimum there is some minor configuration required to ensure the app works as expected.

#### Database connection
The application requires a running [CouchDB](http://couchdb.apache.org) database using this [design document](https://bitbucket.org/redbacks6/couched-rails/src/master/db/design_doc.js).  The address of the database must be updated in the following locations:

* [application_controller.rb](https://bitbucket.org/redbacks6/couched-rails/src/master/app/assets/controllers/application_controller.rb) - Manages the Rails connection with the database.
* [api.js](https://bitbucket.org/redbacks6/couched-rails/src/master/app/assets/javascripts/api.js) - Manage the client connection with the database.

The Tweets are slightly modified from those provided by Twitter when accessing via the Streaming API, namely they have been appended with the following tagged fields:

* Tagged words - I used a few basic packages from the [Natural Language Toolkit](http://www.nltk.org). Most basically this could be achieved with a .split(" ") type function.
    * clean_text - array containing tokenised words. This is used for word counts. Removal of stop words is recommended.
* Sentiment - I used [TextBlob](http://textblob.readthedocs.org/en/latest/) for tagged sentiment data
    * sentiment_polarity - a float from -1 (negative) to 1 (positive). This is used for sentiment analysis.
    * sentiment_subjectivity - a float from 0 (objective) to 1 (subjective). This is not used at the present time.

This can be done in any way you chose or you can use the [harvester script](https://bitbucket.org/redbacks6/research_project) which I prepared for this project.  Not that this is in a separate repository.

#### Enviroment Variables
The for full functionality the app requires access to Twitter's API keys. These can be generated using [Twitter Application Management](https://apps.twitter.com).
The app accesses the keys using environment (ENV) variables, as this is a more secure way to store passwords and keys required by the app. Specifically it is looking for the following keys:

* "TWITTER_CONSUMER_KEY1"
* "TWITTER_CONSUMER_SECRET1"
* "TWITTER_ACCESS_TOKEN1"
* "TWITTER_ACCESS_SECRET1"

Setting the keys as ENV variables will vary depending on deployment method so read the docs relevant to your specific deployment platform.

## Usage Instructions
Once deployed simply connect to the application using a web browser, i.e. enter in the IP address or domain name into the address bar.

## Troubleshooting
If an issue arises that are not addressed here, or via a google/stackoverflow search, then feel free to contact me directly to discuss.

## Compatibility
This application has been designed and tested to work with Safari.  Not other browsers have been tested and while the application should work on all modern browsers compatibility is not guaranteed.

Lastly I used this excellent [Markdown language cheat sheet](https://bitbucket.org/tutorials/markdowndemo) to pull together this README.  I've left the link here mainly for my benefit but feel free to check it out.